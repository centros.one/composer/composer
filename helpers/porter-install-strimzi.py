import argparse
import json
import tempfile
import os
import subprocess
import sys
import base64


def create_credentials(path):
    data = {
        "credentials": [
            {
                "name": "kubeconfig",
                "source": {
                    "path": args.kubeconfig
                }
            }
        ]
    }

    cfile = os.path.join(path, "kubeconfig.json")
    with open(cfile, 'w') as cf:
        json.dump(data, cf)
    if args.dryrun:
        print(json.dumps(data))
    return cfile


def create_parameters(path, name, data):
    pfile = os.path.join(path, f"{name}.json")
    pdata = {
        "parameters": [{
            "name": k,
            "source": {
                "value": json.dumps(v) if not isinstance(v, str) else v
            }
        } for k, v in data.items()]
    }

    with open(pfile, 'w') as pf:
        json.dump(pdata, pf)
    return pfile


def print_command(cmd, params):
    print("\n" + " ".join(cmd))
    with open(params, 'r') as fin:
        print("\n" + fin.read())
    print("\n---\n")


def do_install(install, credential_file, path):
    parameter_file = create_parameters(path, f"{install['name']}-install", install['parameters'])
    cmd = ["porter", "install", "--force", install['name'],
           "--reference", install['image'],
           "--cred", credential_file,
           "--parameter-set", parameter_file]
    print(f"Install service '{install['name']}'")
    if args.dryrun:
        print_command(cmd, parameter_file)
    else:
        subprocess.run(cmd, check=True, stderr=subprocess.STDOUT)


def create_topic(topic, credential_file, path):
    namespace = args.strimzi_namespace if args.strimzi_namespace else topic["namespace"]
    manifest = {
        "apiVersion": "kafka.strimzi.io/v1beta2",
        "kind": "KafkaTopic",
        "metadata": {
            "name": topic["name"],
            "namespace": namespace,
            "labels": {
                "strimzi.io/cluster": args.kafka_cluster
            }
        },
        "spec": {
            "topicName": topic["name"],
            "partitions": topic["partitions"],
            "replicas": topic["replication-factor"],
            "config": topic["config"]
        }
    }

    parameters = {
        "manifest-json": json.dumps(manifest),
        "resource-ready-template": '{{range $k, $v := .status.conditions}}{{if eq .type "Ready"}}{{printf "%s=%s" .type .status}}{{end}}{{end}}',
        "resource-ready-value": "Ready=True"
    }

    parameter_file = create_parameters(path, f"{topic['name']}-topic", parameters)
    cmd = ["porter", "install", "--force", topic["name"],
           "--reference", args.kubeapply_image,
           "--cred", credential_file,
           "--parameter-set", parameter_file]
    print(f"Create topic '{topic['name']}'")
    if args.dryrun:
        print_command(cmd, parameter_file)
    else:
        subprocess.run(cmd, check=True, stderr=subprocess.STDOUT)


def create_schema(schema, credential_file, path, additional_params):
    parameters = {
        **additional_params,
        "schema-registry-url": schema["schema-registry-url"],
        "schema-definition": schema["schema-definition"]
    }
    if "namespace" in schema:
        parameters["namespace"] = schema["namespace"]

    parameter_file = create_parameters(path, f"{schema['subject']}-schema", parameters)
    cmd = ["porter", "install", "--force", schema["subject"],
           "--reference", args.schema_deployment_image,
           "--cred", credential_file,
           "--parameter-set", parameter_file]
    print(f"Register schema for subject '{schema['subject']}'")
    if args.dryrun:
        print_command(cmd, parameter_file)
    else:
        subprocess.run(cmd, check=True, stderr=subprocess.STDOUT)


def create_connector(connector, credential_file, path):
    namespace = args.strimzi_namespace if args.strimzi_namespace else connector["namespace"]
    connector_config = connector["config"]
    connector_class = connector_config.pop("connector.class", None)
    tasks_max = connector_config.pop("tasks.max", None)
    manifest = {
        "apiVersion": "kafka.strimzi.io/v1beta2",
        "kind": "KafkaConnector",
        "metadata": {
            "name": connector["name"],
            "namespace": namespace,
            "labels": {
                "strimzi.io/cluster": args.connect_cluster
            }
        },
        "spec": {
            "class": connector_class,
            "config": connector_config
        }
    }
    if tasks_max:
        manifest["spec"]["tasksMax"] = tasks_max

    parameters = {
        "manifest-json": json.dumps(manifest),
        "resource-ready-template": '{{range $k, $v := .status.conditions}}{{if eq .type "Ready"}}{{printf "%s=%s" .type .status}}{{end}}{{end}}',
        "resource-ready-value": "Ready=True"
    }

    parameter_file = create_parameters(path, f"{connector['name']}-connector", parameters)
    cmd = ["porter", "install", "--force", connector["name"],
           "--reference", args.kubeapply_image,
           "--cred", credential_file,
           "--parameter-set", parameter_file]
    print(f"Create connector '{connector['name']}'")
    if args.dryrun:
        print_command(cmd, parameter_file)
    else:
        subprocess.run(cmd, check=True, stderr=subprocess.STDOUT)


def b64(string):
    return base64.b64encode(string.encode("utf-8")).decode("utf-8")


def create_ips(ips, credential_file, path):
    data = {
        "auths": {
            ips["registry"]: {
                "username": ips["username"],
                "password": ips["password"],
                "email": "",
                "auth": b64(f"{ips['username']}:{ips['password']}")
            }
        }
    }

    print(data)

    manifest = {
        "apiVersion": "v1",
        "kind": "Secret",
        "metadata": {
            "name": ips['name']
        },
        "type": "kubernetes.io/dockerconfigjson",
        "data": {
            ".dockerconfigjson": b64(json.dumps(data))
        }
    }
    if "namespace" in ips:
        manifest["metadata"]["namespace"] = ips["namespace"]

    parameter_file = create_parameters(path, f"{ips['name']}-ips", {"manifest-json": json.dumps(manifest)})
    cmd = ["porter", "install", "--force", ips["name"],
           "--reference", args.kubeapply_image,
           "--cred", credential_file,
           "--parameter-set", parameter_file]
    print(f"Create image pull secret '{ips['name']}'")
    if args.dryrun:
        print_command(cmd, parameter_file)
    else:
        subprocess.run(cmd, check=True, stderr=subprocess.STDOUT)


def get_param_dict(args_list):
    result = {}
    for arg in args_list:
        key, value = arg.split('=', 1)
        result[key] = value
    return result


parser = argparse.ArgumentParser(add_help=False)
parser.add_argument('input')
parser.add_argument('kubeconfig')
parser.add_argument('schema_deployment_image')
parser.add_argument('kubeapply_image')
parser.add_argument('kafka_cluster')
parser.add_argument('connect_cluster')
parser.add_argument('--strimzi-namespace')
parser.add_argument('--schema-deployment-param', action='extend', nargs="+")
parser.add_argument('--dry-run', dest='dryrun', action='store_true')
args = parser.parse_args()

schema_deployment_params = {}
if args.schema_deployment_param:
    schema_deployment_params = get_param_dict(args.schema_deployment_param)

with open(args.input) as infile:
    config = json.load(infile)

with tempfile.TemporaryDirectory(prefix="porter-install-") as workdir:
    credfile = create_credentials(workdir)

    try:
        secrets = config['imagepullsecrets']
        for i in secrets:
            create_ips(i, credfile, workdir)

        topics = config['topics']
        for t in topics:
            create_topic(t, credfile, workdir)

        schemas = config['schemas']
        for s in schemas:
            create_schema(s, credfile, workdir, schema_deployment_params)

        installations = config['installations']
        for i in installations:
            do_install(i, credfile, workdir)

        connectors = config['connectors']
        for c in connectors:
            create_connector(c, credfile, workdir)
    except subprocess.CalledProcessError as ex:
        sys.exit(1)
