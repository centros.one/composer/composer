import argparse
import json
import tempfile
import os
import subprocess
import sys


def create_credentials(path):
    data = {
        "credentials": [
            {
                "name": "kubeconfig",
                "source": {
                    "path": args.kubeconfig
                }
            }
        ]
    }

    cfile = os.path.join(path, "kubeconfig.json")
    with open(cfile, 'w') as cf:
        json.dump(data, cf)
    if args.dryrun:
        print(json.dumps(data))
    return cfile


def create_parameters(path, name, data):
    pfile = os.path.join(path, f"{name}.json")
    pdata = {
        "parameters": [{
            "name": k,
            "source": {
                "value": json.dumps(v) if not isinstance(v, str) else v
            }
        } for k, v in data.items()]
    }

    with open(pfile, 'w') as pf:
        json.dump(pdata, pf)
    return pfile


def print_command(cmd, params):
    print("\n" + " ".join(cmd))
    with open(params, 'r') as fin:
        print("\n" + fin.read())
    print("\n---\n")


def do_install(install, credential_file, path):
    parameter_file = create_parameters(path, f"{install['name']}-install", install['parameters'])
    cmd = ["porter", "install", "--force", install['name'],
           "--reference", install['image'],
           "--cred", credential_file,
           "--parameter-set", parameter_file]
    print(f"Install service '{install['name']}'")
    if args.dryrun:
        print_command(cmd, parameter_file)
    else:
        subprocess.run(cmd, check=True, stderr=subprocess.STDOUT)


def create_topic(topic, credential_file, path):
    parameters = {
        "kafka-bootstrap-server": topic["bootstrap-server"],
        "topic-partitions": topic["partitions"],
        "topic-replicas": topic["replication-factor"],
        "topic-config-json": json.dumps(topic["config"])
    }
    if "namespace" in topic:
        parameters["namespace"] = topic["namespace"]

    parameter_file = create_parameters(path, f"{topic['name']}-topic", parameters)
    cmd = ["porter", "install", "--force", topic["name"],
           "--reference", args.topic_deployment_image,
           "--cred", credential_file,
           "--parameter-set", parameter_file]
    print(f"Create topic '{topic['name']}'")
    if args.dryrun:
        print_command(cmd, parameter_file)
    else:
        subprocess.run(cmd, check=True, stderr=subprocess.STDOUT)


def create_schema(schema, credential_file, path):
    parameters = {
        "schema-registry-url": schema["schema-registry-url"],
        "schema-definition": schema["schema-definition"]
    }
    if "namespace" in schema:
        parameters["namespace"] = schema["namespace"]

    parameter_file = create_parameters(path, f"{schema['subject']}-schema", parameters)
    cmd = ["porter", "install", "--force", schema["subject"],
           "--reference", args.schema_deployment_image,
           "--cred", credential_file,
           "--parameter-set", parameter_file]
    print(f"Register schema for subject '{schema['subject']}'")
    if args.dryrun:
        print_command(cmd, parameter_file)
    else:
        subprocess.run(cmd, check=True, stderr=subprocess.STDOUT)


def create_connector(connector, credential_file, path):
    parameters = {
        "connect-url": connector["connect-url"],
        "connector-config-json": json.dumps(connector["config"])
    }
    if "namespace" in connector:
        parameters["namespace"] = connector["namespace"]

    parameter_file = create_parameters(path, f"{connector['name']}-connector", parameters)
    cmd = ["porter", "install", "--force", connector["name"],
           "--reference", args.connector_deployment_image,
           "--cred", credential_file,
           "--parameter-set", parameter_file]
    print(f"Create connector '{connector['name']}'")
    if args.dryrun:
        print_command(cmd, parameter_file)
    else:
        subprocess.run(cmd, check=True, stderr=subprocess.STDOUT)


def create_ips(ips, credential_file, path):
    parameters = {
        k: ips[k] for k in [ "registry", "username", "password", "namespace" ] if k in ips
    }
    parameter_file = create_parameters(path, f"{ips['name']}-ips", parameters)
    cmd = ["porter", "install", "--force", ips["name"],
           "--reference", args.imagepullsecret_image,
           "--cred", credential_file,
           "--parameter-set", parameter_file]
    print(f"Create image pull secret '{ips['name']}'")
    if args.dryrun:
        print_command(cmd, parameter_file)
    else:
        subprocess.run(cmd, check=True, stderr=subprocess.STDOUT)


parser = argparse.ArgumentParser(add_help=False)
parser.add_argument('input')
parser.add_argument('kubeconfig')
parser.add_argument('topic_deployment_image')
parser.add_argument('schema_deployment_image')
parser.add_argument('connector_deployment_image')
parser.add_argument('imagepullsecret_image')
parser.add_argument('--dry-run', dest='dryrun', action='store_true')
args = parser.parse_args()

with open(args.input) as infile:
    config = json.load(infile)

with tempfile.TemporaryDirectory(prefix="porter-install-") as workdir:
    credfile = create_credentials(workdir)

    try:
        secrets = config['imagepullsecrets']
        for i in secrets:
            create_ips(i, credfile, workdir)

        topics = config['topics']
        for t in topics:
            create_topic(t, credfile, workdir)

        schemas = config['schemas']
        for s in schemas:
            create_schema(s, credfile, workdir)

        installations = config['installations']
        for i in installations:
            do_install(i, credfile, workdir)

        connectors = config['connectors']
        for c in connectors:
            create_connector(c, credfile, workdir)
    except subprocess.CalledProcessError as ex:
        sys.exit(1)
