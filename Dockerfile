FROM maven:3.6.3-openjdk-11
WORKDIR /usr/src/build
COPY . .
RUN mvn package -f pom.xml

FROM openjdk:11

RUN apt-get update \
    && apt-get install -y --no-install-recommends gosu \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

ARG COMMIT_SHA
ENV COMMIT_SHA ${COMMIT_SHA}
ARG COMMIT_REF_SLUG
ENV COMMIT_REF_SLUG ${COMMIT_REF_SLUG}

COPY --from=0 /usr/src/build/target/composer-*-shaded.jar composer-shaded.jar
COPY helpers/porter-install.py porter-install.py
COPY helpers/porter-install-strimzi.py porter-install-strimzi.py
COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
