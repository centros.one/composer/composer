#!/bin/bash

# If command starts with an option, prepend the composer command
# This allows users to add command-line options without
# needing to specify the "composer" command
if [ "${1:0:1}" = '-' ]; then
  set -- java -jar /usr/src/app/composer-shaded.jar "$@"
fi

if [ "$1" = "composer" ];
then
  shift
  set -- java -jar /usr/src/app/composer-shaded.jar "$@"
else
  exec "$@"
fi
