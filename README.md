## Use

```bash
docker run --rm -i -v `pwd`:/mnt registry.gitlab.com/centros.one/composer/composer:0-2-0 --ccf-file /mnt/ccf.json --repo-file /mnt/repo.json --output-file /mnt/vars.yml --kafka-bootstrap kafka-$NAMESPACE-cp-kafka:9092 --kafka-schemaregistry http://kafka-$NAMESPACE-cp-schema-registry:8081 --kafka-clustername kafka-$NAMESPACE --kafka-connecturl=http://kafka-$NAMESPACE-cp-kafka-connect:8083 --kafka-connect-clustername=kafka-$NAMESPACE-connect
```
