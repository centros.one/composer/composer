package one.centros.generator;

public enum GeneratorType {
    GENERIC("default", GenericGenerator.class);

    private final String displayName;
    private final Class<? extends IGenerator> generatorClass;

    GeneratorType(String displayName, Class<? extends IGenerator> generatorClass) {
        this.displayName = displayName;
        this.generatorClass = generatorClass;
    }

    public IGenerator getInstance() throws ReflectiveOperationException {
        return generatorClass.getConstructor().newInstance();
    }

    @Override
    // Note that picocli uses the result of the toString method to identify the values of enums (and display the
    // completion candidates)
    public String toString() {
        return displayName;
    }
}
