package one.centros.generator;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class GeneratorConfig extends HashMap<String,String> {
    public static String KEY_KAFKA_BOOTSTRAP_SERVER = "kafka-bootstrap-server";
    public static String KEY_SCHEMA_REGISTRY_URL = "schema-registry-url";
    public static String KEY_CONNECT_URL = "kafka-connect-url";
    public static String KEY_TARGET_NAMESPACE = "target-namespace";

    public GeneratorConfig(Map<String, String> map) {
        super(map != null ? map : Collections.emptyMap());
    }
}
