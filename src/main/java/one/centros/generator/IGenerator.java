package one.centros.generator;

import one.centros.Composer;
import one.centros.ServiceResolver;
import one.centros.data.CCFDataSource;

import java.nio.file.Path;

public interface IGenerator {
    void configure(GeneratorConfig config);

    void setOutput(Path output);
    Path getOutput();

    boolean generate(CCFDataSource ccf, ServiceResolver serviceResolver, Composer.AuthMethod authMethod);
}
