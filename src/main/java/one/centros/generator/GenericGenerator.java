package one.centros.generator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import one.centros.AuthData;
import one.centros.Composer;
import one.centros.Pair;
import one.centros.ServiceResolver;
import one.centros.Util;
import one.centros.data.CCFDataSource;
import one.centros.model.*;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class GenericGenerator extends AbstractGenerator {
    private static final Logger logger = LoggerFactory.getLogger(GenericGenerator.class);

    private static final String AVRO_CONVERTER_CLASS = "io.confluent.connect.avro.AvroConverter";

    private final ObjectMapper mapper;

    private final Map<String, String> ipsnames = new HashMap<>();

    public GenericGenerator() {
        mapper = new ObjectMapper();
    }

    @Override
    protected Set<String> requiredConfig() {
        HashSet<String> rc = new HashSet<>();
        rc.add(GeneratorConfig.KEY_KAFKA_BOOTSTRAP_SERVER);
        rc.add(GeneratorConfig.KEY_SCHEMA_REGISTRY_URL);
        return rc;
    }

    @Override
    protected String expectedExtension() {
        return ".json";
    }

    @Override
    public boolean generate(CCFDataSource ccf, ServiceResolver serviceResolver, Composer.AuthMethod authMethod) {
        ObjectNode root = mapper.createObjectNode();
        root.set("imagepullsecrets", mapper.createArrayNode().addAll(
                generatePullsecrets(ccf.getAllServices(), serviceResolver, authMethod)));
        root.set("installations", mapper.createArrayNode().addAll(
                generateServices(ccf, serviceResolver, authMethod)));
        root.set("topics", mapper.createArrayNode().addAll(
                generateTopicDeployment(ccf.getAllTopics())));
        root.set("schemas", mapper.createArrayNode().addAll(
                generateSchemaDeployment(ccf.getAllTopics())));
        root.set("connectors", mapper.createArrayNode().addAll(
                generateConnectorDeployment(ccf.getAllServices(), ccf.getAllTopics())));

        try (OutputStream ostream = Files.newOutputStream(output, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE)) {
            mapper.writerWithDefaultPrettyPrinter().writeValue(ostream, root);
        } catch (IOException ex) {
            logger.error(String.format("Error writing output file '%s'", output), ex);
            return false;
        }
        return true;
    }

    private static final Pattern NONLATIN = Pattern.compile("[^\\w-]");
    private static final Pattern WHITESPACE = Pattern.compile("[\\s]");

    public static String toSlug(String input) {
        String nowhitespace = WHITESPACE.matcher(input).replaceAll("-");
        String normalized = Normalizer.normalize(nowhitespace, Normalizer.Form.NFD);
        String slug = NONLATIN.matcher(normalized).replaceAll("");
        return slug.toLowerCase(Locale.ENGLISH);
    }

    private Collection<JsonNode> generatePullsecrets(List<ServiceInstance> allServices, ServiceResolver serviceResolver, Composer.AuthMethod authMethod) {
        ArrayList<JsonNode> nodes = new ArrayList<>();

        if (authMethod == Composer.AuthMethod.COPY) {
            Map<String, AuthData.Item> byHost = allServices.stream()
                    .map(serviceResolver::findServiceByType)
                    .filter(IServiceDefinition::needsAuth)
                    .map(sd -> serviceResolver.getAuth(sd.getServiceTypeRef()))
                    .collect(Collectors.groupingBy(
                            AuthData.Item::host,
                            new Util.First<>()
                    ));

            String namespace = config.get(GeneratorConfig.KEY_TARGET_NAMESPACE);

            for (Map.Entry<String, AuthData.Item> item : byHost.entrySet()) {
                AuthData.Item auth = item.getValue();
                ObjectNode ipsNode = mapper.createObjectNode();
                nodes.add(ipsNode);

                String ipsname = "ips-" + toSlug(auth.host());
                ipsnames.put(auth.host(), ipsname);
                ipsNode.put("name", ipsname);
                if (namespace != null) {
                    ipsNode.put("namespace", namespace);
                }
                ipsNode.put("registry", auth.host());
                ipsNode.put("username", auth.user());
                ipsNode.put("password", auth.pass());
            }
        }

        return nodes;
    }

    private Collection<JsonNode> generateServices(CCFDataSource ccf, ServiceResolver serviceResolver, Composer.AuthMethod authMethod) {
        ArrayList<JsonNode> nodes = new ArrayList<>();
        Map<String, Set<Topic>> topicsByService = new HashMap<>();
        for (Topic topic : ccf.getAllTopics()) {
            for (SocketReference socketRef : topic.getSockets()) {
                topicsByService.computeIfAbsent(socketRef.getServiceName(), k -> new HashSet<>()).add(topic);
            }
        }

        String bootstrap = config.get(GeneratorConfig.KEY_KAFKA_BOOTSTRAP_SERVER);
        String registry = config.get(GeneratorConfig.KEY_SCHEMA_REGISTRY_URL);
        String namespace = config.get(GeneratorConfig.KEY_TARGET_NAMESPACE);

        for(ServiceInstance service : ccf.getAllServices()) {
            if (service.isProvided()) {
                // service is 'provided' -> not handled by composer, don't generate an installation entry
                continue;
            }
            IServiceDefinition serviceDef = serviceResolver.findServiceByType(service);
            ObjectNode serviceNode = mapper.createObjectNode();
            serviceNode.put("name", service.getName());
            ServiceTypeRef typeRef = serviceDef.getServiceTypeRef();
            Set<String> paramNames = serviceDef.getParameters().stream().map(Parameter::getName).collect(Collectors.toSet());
            String image = String.format("%s/%s:v%s", typeRef.getHost(), typeRef.getTypeName(), serviceDef.getVersion());

            ObjectNode instanceNode = serviceNode.withArray("instances").addObject();
            instanceNode.put("name", service.getName());
            instanceNode.put("image", image);
            nodes.add(instanceNode);

            ObjectNode paramsNode = instanceNode.with("parameters");
            if (service.getParameterJson() instanceof ObjectNode) {
                paramsNode.setAll((ObjectNode) service.getParameterJson());
            }

            if (paramNames.contains("kafka-bootstrap")) {
                paramsNode.put("kafka-bootstrap", bootstrap);
            }
            if (paramNames.contains("schema-registry-url")) {
                paramsNode.put("schema-registry-url", registry);
            }
            if (namespace != null) {
                if (paramNames.contains("namespace")) {
                    paramsNode.put("namespace", namespace);
                } else {
                    logger.warn("CNAB \"{}\" for service \"{}\" does not have a namespace parameter", image, service.getName());
                }
            }

            if (serviceDef.needsAuth() && authMethod == Composer.AuthMethod.COPY) {
                paramsNode.put("image-pull-secret", ipsnames.get(serviceDef.getServiceTypeRef().getHost()));
            }

            List<Pair<SocketReference, Topic>> input = new ArrayList<>();
            List<Pair<SocketReference, Topic>> output = new ArrayList<>();
            Set<Topic> topics = topicsByService.get(service.getName());
            if (topics != null) {
                for (Topic topic : topics) {
                    for (SocketReference socketRef : topic.getSockets()) {
                        if (socketRef.getServiceName().equals(service.getName()))
                            (socketRef.getDirection() == InOut.IN ? input : output)
                                    .add(new Pair<>(socketRef, topic));
                    }
                }
            }
            if (!input.isEmpty() || !output.isEmpty()) {
                if (!input.isEmpty()) {
                    Map<String, List<String>> socketTopicMap = new HashMap<>();
                    for (Pair<SocketReference, Topic> connection : input) {
                        String socketName = String.format("sockets-input-%s-topic", connection.left().getSocketName());
                        if (paramNames.contains(socketName)) {
                            socketTopicMap.computeIfAbsent(socketName, k -> new ArrayList<>())
                                    .add(connection.right().getName());
                        } else {
                            logger.warn("CNAB \"{}\" for service \"{}\" does not have input socket parameter: {}", image, service.getName(), socketName);
                        }
                    }
                    for(Map.Entry<String, List<String>> item : socketTopicMap.entrySet()) {
                        paramsNode.put(item.getKey(), String.join(",", item.getValue()));
                    }
                }
                if (!output.isEmpty()) {
                    for (Pair<SocketReference, Topic> connection : output) {
                        String name = String.format("sockets-output-%s-topic", connection.left().getSocketName());
                        if (paramNames.contains(name)) {
                            paramsNode.put(name, connection.right().getName());
                        } else {
                            logger.warn("CNAB \"{}\" for service \"{}\" does not have output socket parameter: {}", image, service.getName(), name);
                        }
                    }
                }
            }
        }
        return nodes;
    }

    private String getSchemaString(SchemaSource source) {
        String schema;
        try (InputStream istream = source.openStream()) {
            schema = new String(IOUtils.toByteArray(istream), StandardCharsets.UTF_8);
        } catch (IOException ex) {
            // this should never happen because we must have been able to successfully read the schema at least once
            // already during the validation step if we ever managed to get this far
            logger.error("Unexpected error", ex);
            return "";
        }

        try {
            // minify the schema json (removing line feeds, indentation etc.) by parsing and re-rendering it
            return mapper.writeValueAsString(mapper.readTree(schema));
        } catch (JsonProcessingException ex) {
            // see above
            logger.error("Unexpected error", ex);
            return "";
        }
    }

    private enum SchemaType {
        KEY("-key", Pair::left), VALUE("-value", Pair::right);

        private final String suffix;
        private final Function<Pair<SchemaSource, SchemaSource>, SchemaSource> selector;

        SchemaType(String suffix, Function<Pair<SchemaSource, SchemaSource>, SchemaSource> selector) {
            this.suffix = suffix;
            this.selector = selector;
        }

        public String getSuffix() {
            return suffix;
        }

        public SchemaSource select(Pair<SchemaSource, SchemaSource> pair) {
            return selector.apply(pair);
        }
    }

    private Collection<JsonNode> generateSchemaDeployment(List<Topic> allTopics) {
        ArrayList<JsonNode> nodes = new ArrayList<>();

        String registry = config.get(GeneratorConfig.KEY_SCHEMA_REGISTRY_URL);
        String namespace = config.get(GeneratorConfig.KEY_TARGET_NAMESPACE);

        for (Topic topic : allTopics) {
            Pair<SchemaSource, SchemaSource> schemas = topic.getCanonicalSchemas();
            for (SchemaType type : SchemaType.values()) {
                ObjectNode subjectNode = mapper.createObjectNode();
                subjectNode.put("subject", topic.getName() + type.getSuffix());
                if (namespace != null) {
                    subjectNode.put("namespace", namespace);
                }

                subjectNode.put("schema-registry-url", registry);
                subjectNode.put("schema-definition", getSchemaString(type.select(schemas)));

                nodes.add(subjectNode);
            }
        }

        return nodes;
    }

    private Collection<JsonNode> generateTopicDeployment(List<Topic> allTopics) {
        ArrayList<JsonNode> nodes = new ArrayList<>();

        String bootstrap = config.get(GeneratorConfig.KEY_KAFKA_BOOTSTRAP_SERVER);
        String namespace = config.get(GeneratorConfig.KEY_TARGET_NAMESPACE);

        for(Topic topic : allTopics) {
            ObjectNode topicNode = mapper.createObjectNode();
            topicNode.put("name", topic.getName());
            if (namespace != null) {
                topicNode.put("namespace", namespace);
            }

            ObjectNode configNode = mapper.createObjectNode();
            configNode.setAll((ObjectNode) topic.getConfig());
            int partitions = configNode.get("partitions").asInt();
            configNode.remove("partitions");
            int replicas = configNode.get("replicationFactor").asInt();
            configNode.remove("replicationFactor");

            topicNode.put("partitions", partitions);
            topicNode.put("replication-factor", replicas);
            topicNode.put("bootstrap-server", bootstrap);
            topicNode.set("config", configNode);
            nodes.add(topicNode);
        }

        return nodes;
    }

    private Collection<JsonNode> generateConnectorDeployment(List<ServiceInstance> services, List<Topic> topics) {
        Map<String, Pair<ServiceInstance, Connector>> allConnectors = services.stream()
                .flatMap(s -> s.getConnectors().stream()
                        .map(c -> new Pair<>(s, c)))
                .collect(Collectors.toMap(p -> p.right().getName(), Function.identity()));

        if (allConnectors.isEmpty()) {
            return Collections.emptyList();
        }

        Map<Pair<String, String>, AdditionalSocketConnect> additionalSocketMap = services.stream()
                .flatMap(s -> s.getAdditionalSockets(InOut.IN).stream()
                        .filter(as -> as instanceof AdditionalSocketConnect)
                        .map(as -> (AdditionalSocketConnect) as)
                        .map(as -> new Pair<>(new Pair<>(s.getName(), as.getName()), as)))
                .collect(Collectors.toMap(Pair::left, Pair::right));
        Map<Connector, List<Topic>> relevantTopics = new HashMap<>();

        for (Topic topic : topics) {
            for (SocketReference socketReference : topic.getSockets(InOut.IN)) {
                Pair<String, String> key = new Pair<>(socketReference.getServiceName(), socketReference.getSocketName());
                if (additionalSocketMap.containsKey(key)) {
                    AdditionalSocketConnect additionalSocket = additionalSocketMap.get(key);
                    Connector connector = allConnectors.get(additionalSocket.getConnectorRef()).right();
                    relevantTopics.computeIfAbsent(connector, c -> new LinkedList<>());
                    relevantTopics.get(connector).add(topic);
                }
            }
        }

        String connectUrl = config.get(GeneratorConfig.KEY_CONNECT_URL);
        String namespace = config.get(GeneratorConfig.KEY_TARGET_NAMESPACE);

        if (connectUrl == null) {
            logger.error("Service configuration includes connectors, but Kafka Connect URL is not set");
            System.exit(1);
        }

        ArrayList<JsonNode> nodes = new ArrayList<>();

        for (Pair<ServiceInstance, Connector> pair : allConnectors.values()) {
            ServiceInstance serviceInstance = pair.left();
            Connector connector = pair.right();
            ObjectNode connectorNode = mapper.createObjectNode();
            connectorNode.put("name", serviceInstance.getName() + '-' + connector.getName());
            if (namespace != null) {
                connectorNode.put("namespace", namespace);
            }

            connectorNode.put("connect-url", connectUrl);

            ObjectNode configNode = mapper.createObjectNode();

            String registry = config.get(GeneratorConfig.KEY_SCHEMA_REGISTRY_URL);
            configNode.put("key.converter", AVRO_CONVERTER_CLASS);
            configNode.put("key.converter.schema.registry.url", registry);
            configNode.put("value.converter", AVRO_CONVERTER_CLASS);
            configNode.put("value.converter.schema.registry.url", registry);

            if (relevantTopics.containsKey(connector)) {
                configNode.put("topics", relevantTopics.get(connector).stream().map(Topic::getName).collect(Collectors.joining(",")));
            }

            configNode.setAll((ObjectNode) connector.getConfig());
            connectorNode.set("config", configNode);

            nodes.add(connectorNode);
        }
        return nodes;
    }
}
