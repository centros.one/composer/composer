package one.centros.generator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.util.Set;

public abstract class AbstractGenerator implements IGenerator {
    private static final Logger logger = LoggerFactory.getLogger(AbstractGenerator.class);

    protected Path output;
    protected GeneratorConfig config;

    protected abstract Set<String> requiredConfig();
    protected abstract String expectedExtension();

    @Override
    public void setOutput(Path output) {
        if (!output.toString().endsWith(expectedExtension())) {
            logger.warn(String.format("Output filename '%s' does not end with the extension '%s' expected by the chosen generator.",
                    output, expectedExtension()));
        }
        this.output = output;
    }

    @Override
    public Path getOutput() {
        return output;
    }

    @Override
    public void configure(GeneratorConfig config) {
        Set<String> missing = requiredConfig();
        missing.removeAll(config.keySet());
        if (!missing.isEmpty()) {
            logger.error(String.format("%s configuration requires the following values: %s",
                    this.getClass().getSimpleName(), String.join(", ", missing)));
            System.exit(1);
        }
        this.config = config;
    }
}
