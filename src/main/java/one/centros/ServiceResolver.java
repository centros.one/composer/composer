package one.centros;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vdurmont.semver4j.Requirement;
import com.vdurmont.semver4j.Semver;
import com.vdurmont.semver4j.SemverException;
import one.centros.model.IServiceDefinition;
import one.centros.model.ServiceDefinition;
import one.centros.model.ServiceInstance;
import one.centros.model.ServiceTypeRef;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class ServiceResolver {
    private static final Logger logger = LoggerFactory.getLogger(ServiceResolver.class);

    private final AuthData authData;
    private final ObjectMapper mapper = new ObjectMapper();
    private final Map<ServiceTypeRef, IServiceDefinition> cache = new HashMap<>();

    public ServiceResolver(AuthData authData) {
        this.authData = authData;
    }

    public AuthData.Item getAuth(ServiceTypeRef typeRef) {
        return authData.getAuth(typeRef.getHost());
    }

    private void setAuthHeader(HttpGet request, AuthData.Item auth) {
        if (auth != null) {
            String token = auth.user() + ":" + auth.pass();
            request.setHeader(HttpHeaders.AUTHORIZATION, "Basic " + Base64.getEncoder().encodeToString(token.getBytes(StandardCharsets.UTF_8)));
        }
    }

    private Pair<JsonNode, StatusLine> tryFetch(String manifestUrl, AuthData.Item auth) throws IOException {
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpGet request = new HttpGet(manifestUrl);
            setAuthHeader(request, auth);
            try (CloseableHttpResponse response = client.execute(request)) {
                StatusLine statusLine = response.getStatusLine();
                JsonNode manifest = null;
                if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
                    HttpEntity entity = response.getEntity();
                    manifest = mapper.readTree(entity.getContent());
                    EntityUtils.consume(entity);
                }
                return new Pair<>(manifest, response.getStatusLine());
            }
        }
    }

    private Semver parseSemver(String version) {
        try {
            return new Semver(version.replaceAll("^v", ""));
        } catch (SemverException ex) {
            logger.debug(String.format("Found artifact with invalid version number '%s'", version));
            return null;
        }
    }

    private String bestMatch(List<String> tags, String constraint) {
        Requirement req = Requirement.buildNPM(constraint);
        Optional<Semver> max = tags.stream()
                .map(this::parseSemver)
                .filter(v -> v != null && v.satisfies(req))
                .max(Comparator.naturalOrder());
        return max.map(Semver::toString).orElse(null);
    }

    private Pair<String, Boolean> getMatchingTag(ServiceTypeRef typeRef) {
        String taglistUrl = String.format("https://%s/v2/%s/tags/list", typeRef.getHost(), typeRef.getTypeName());
        try {
            for (boolean needsAuth : Arrays.asList(false, true)) {
                AuthData.Item auth = null;
                if (needsAuth) {
                    auth = getAuth(typeRef);
                    if (auth == null) {
                        logger.error(String.format("No authentication info available for repository '%s'", typeRef.getHost()));
                        return null;
                    }
                }
                Pair<JsonNode, StatusLine> fetchResult = tryFetch(taglistUrl, auth);
                if (fetchResult.right().getStatusCode() == HttpStatus.SC_OK) {
                    List<String> tags = Util.stream(fetchResult.left().get("tags"))
                            .map(JsonNode::textValue).collect(Collectors.toList());
                    String tag = bestMatch(tags, typeRef.getVersion());
                    return tag != null ? new Pair<>(tag, needsAuth) : null;
                } else if (fetchResult.right().getStatusCode() == HttpStatus.SC_UNAUTHORIZED && !needsAuth) {
                    // we have not tried with authentication yet, do so (if available)
                    continue;
                }
                logger.error("Failed to fetch service manifest: " + fetchResult.right().toString());
                return null;
            }
        } catch (IOException ex) {
            logger.error("Failed to fetch service tags", ex);
            return null;
        }

        return null;
    }

    private Pair<JsonNode, Boolean> fetchManifests(ServiceTypeRef typeRef) {
        Pair<String, Boolean> pair = getMatchingTag(typeRef);

        if (pair == null) {
            return null;
        }

        String version = pair.left();
        boolean needsAuth = pair.right();

        String manifestUrl = String.format("https://%s/v2/%s/manifests/v%s", typeRef.getHost(), typeRef.getTypeName(), version);

        try {
            Pair<JsonNode, StatusLine> fetchResult = tryFetch(manifestUrl, needsAuth ? getAuth(typeRef) : null);
            if (fetchResult.right().getStatusCode() == HttpStatus.SC_OK) {
                return new Pair<>(fetchResult.left(), needsAuth);
            }
            logger.error("Failed to fetch service manifest: " + fetchResult.right().toString());
            return null;
        } catch (IOException ex) {
            logger.error("Failed to fetch service manifest", ex);
            return null;
        }
    }

    private JsonNode fetchManifest(ServiceTypeRef typeRef, String digest, String mediaType, boolean needsAuth) {
        String manifestUrl = String.format("https://%s/v2/%s/manifests/%s", typeRef.getHost(), typeRef.getTypeName(), digest);
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpGet request = new HttpGet(manifestUrl);
            request.setHeader(HttpHeaders.ACCEPT, mediaType);
            if (needsAuth) {
                setAuthHeader(request, getAuth(typeRef));
            }
            try (CloseableHttpResponse response = client.execute(request)) {
                if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                    logger.error("Failed to fetch service manifest: " + response.getStatusLine().toString());
                    return null;
                }
                HttpEntity entity = response.getEntity();
                JsonNode manifest = mapper.readTree(entity.getContent());
                EntityUtils.consume(entity);
                return manifest;
            }
        } catch (IOException ex) {
            logger.error("Failed to fetch service manifest", ex);
            return null;
        }
    }

    private String fixNulls(String jsonString) {
        // workaround due to https://github.com/getporter/porter/issues/1483
        return jsonString.replaceAll("\"<nil>\"", "null");
    }

    private JsonNode fetchBlob(ServiceTypeRef typeRef, String digest, String mediaType, boolean needsAuth) {
        String manifestUrl = String.format("https://%s/v2/%s/blobs/%s", typeRef.getHost(), typeRef.getTypeName(), digest);
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpGet request = new HttpGet(manifestUrl);
            request.setHeader(HttpHeaders.ACCEPT, mediaType);
            if (needsAuth) {
                setAuthHeader(request, getAuth(typeRef));
            }
            try (CloseableHttpResponse response = client.execute(request)) {
                if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                    logger.error("Failed to fetch service manifest: " + response.getStatusLine().toString());
                    return null;
                }
                HttpEntity entity = response.getEntity();
                String content = IOUtils.toString(entity.getContent(), StandardCharsets.UTF_8);
                JsonNode manifest = mapper.readTree(fixNulls(content));
                EntityUtils.consume(entity);
                return manifest;
            }
        } catch (JsonParseException ex) {
            logger.error(String.format("Service manifest for service '%s' is not a valid JSON document", typeRef));
            return null;
        } catch (IOException ex) {
            logger.error("Failed to fetch service manifest", ex);
            return null;
        }
    }

    private Pair<JsonNode, Boolean> getServiceDefinition(ServiceTypeRef typeRef) {
        Pair<JsonNode, Boolean> pair = fetchManifests(typeRef);
        if (pair != null) {
            JsonNode manifests = pair.left();
            boolean needsAuth = pair.right();
            Map<String, List<JsonNode>> map = byType(manifests);
            List<JsonNode> configNodes = map.get("config");
            if (configNodes != null && !configNodes.isEmpty()) {
                if (configNodes.size() > 1) {
                    logger.warn(String.format("More than 1 service config found in manifests of service '%s'", typeRef.toString()));
                }
                JsonNode configNode = configNodes.get(0);
                String digest = configNode.get("digest").textValue();
                String mediaType = configNode.get("mediaType").textValue();
                JsonNode manifestRef = fetchManifest(typeRef, digest, mediaType, needsAuth);
                configNode = Util.sn(manifestRef, n -> n.get("config"));
                if (configNode != null) {
                    digest = configNode.get("digest").textValue();
                    mediaType = configNode.get("mediaType").textValue();
                    JsonNode blob = fetchBlob(typeRef, digest, mediaType, needsAuth);
                    return new Pair<>(blob, needsAuth);
                }
            }
        }
        return null;
    }

    private Map<String, List<JsonNode>> byType(JsonNode root) {
        return Util.stream(root.get("manifests"))
                .collect(Collectors.groupingBy(
                        n -> Util.sn(n.get("annotations"), a -> a.get("io.cnab.manifest.type"), JsonNode::textValue),
                        Collectors.toList()
                ));
    }

    public IServiceDefinition findServiceByType(ServiceInstance service) {
        ServiceTypeRef typeRef = service.getTypeRef();
        if (service.isProvided()) {
            // "Dummy" service definition for 'provided' services
            return new IServiceDefinition() {
                @Override
                public String getVersion() {
                    return typeRef.getVersion();
                }

                @Override
                public ServiceTypeRef getServiceTypeRef() {
                    return typeRef;
                }
            };
        }

        IServiceDefinition definition = cache.get(typeRef);
        if (definition != null) {
            return definition;
        }

        Pair<JsonNode, Boolean> pair = getServiceDefinition(typeRef);
        if (pair != null) {
            IServiceDefinition def = new ServiceDefinition(typeRef, pair.left(), pair.right());
            cache.put(typeRef, def);
            return def;
        }
        return null;
    }
}
