package one.centros;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.Configuration;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaException;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidationMessage;
import one.centros.data.CCFDataSource;
import one.centros.generator.IGenerator;
import one.centros.model.AdditionalSocket;
import one.centros.model.AdditionalSocketBuiltin;
import one.centros.model.AdditionalSocketConnect;
import one.centros.model.Connector;
import one.centros.model.Credential;
import one.centros.model.IServiceDefinition;
import one.centros.visitor.IAdditionalSocketVisitor;
import one.centros.model.InOut;
import one.centros.model.Parameter;
import one.centros.model.SchemaKind;
import one.centros.model.SchemaSource;
import one.centros.model.ServiceInstance;
import one.centros.model.ServiceTypeRef;
import one.centros.model.SocketDefinition;
import one.centros.model.SocketReference;
import one.centros.model.Topic;
import org.apache.avro.Schema;
import org.apache.avro.SchemaCompatibility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Composer {
    private static final Logger logger = LoggerFactory.getLogger(Composer.class);

    private static final HashSet<String> CCF_SCHEMA_WHITELIST = new HashSet<>();

    public enum AuthMethod {
        NONE, COPY, ASSUME
    }

    static {
        CCF_SCHEMA_WHITELIST.add("https://gitlab.com/centros.one/composer/schemas/-/raw/0.1.0/ccf-schema.json");
        CCF_SCHEMA_WHITELIST.add("https://gitlab.com/centros.one/composer/schemas/-/raw/0.2.0/ccf-schema.json");

        Configuration.setDefaults(new JsonPathConfig());
    }

    private final Path ccfFile;
    private final boolean skipValidation;
    private final IGenerator generator;
    private final AuthMethod authMethod;
    private final ServiceResolver serviceResolver;
    private final Map<SchemaSource, Schema> avroSchemaCache = new HashMap<>();

    private CCFDataSource ccfDataSource;

    public Composer(Path ccfFile, AuthData authData, boolean skipValidation, IGenerator generator, AuthMethod authMethod) {
        this.ccfFile = ccfFile;
        this.skipValidation = skipValidation;
        this.generator = generator;
        this.authMethod = authMethod;

        serviceResolver = new ServiceResolver(authData);
    }

    private URI getSchemaUri(JsonNode ccf) {
        JsonNode node = ccf.get("$schema");
        String schemaText = Util.sn(node, JsonNode::textValue);
        if (schemaText == null || schemaText.isEmpty()) {
            logger.error("No valid schema reference found in CCF file");
            System.exit(1);
        }

        if (!CCF_SCHEMA_WHITELIST.contains(schemaText)) {
            logger.error(String.format("CCF references unknown schema '%s'", schemaText));
            System.exit(1);
        }

        URI schemaUri = null;
        try {
            schemaUri = new URI(schemaText);
        } catch (URISyntaxException ex) {
            logger.error(String.format("CCF schema reference '%s' is not a valid URI", schemaText), ex);
            System.exit(1);
        }
        return schemaUri;
    }

    private static JsonSchema getJsonSchema(URI schemaUri) {
        JsonSchema schema = null;
        try {
            schema = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V7).getSchema(schemaUri);
        } catch (JsonSchemaException ex) {
            logger.error(String.format("Failed to retrieve json schema '%s'", schemaUri), ex);
            System.exit(1);
        }
        return schema;
    }

    private boolean verifyServicesExist() {
        boolean result = true;
        for (ServiceInstance service : ccfDataSource.getAllServices()) {
            if (service.isProvided()) {
                logger.info("Service '%s' is marked as 'provided' - no existence check", service.getName());
                continue;
            }

            ServiceTypeRef typeRef;
            try {
                typeRef = service.getTypeRef();
            } catch (IllegalArgumentException ex) {
                logger.error(ex.getMessage());
                result = false;
                continue;
            }

            IServiceDefinition definition = serviceResolver.findServiceByType(service);
            if (definition == null) {
                logger.error(String.format("Service '%s' references service type '%s' in registry '%s' @ version '%s' - not found",
                        service.getName(), typeRef.getTypeName(), typeRef.getHost(), typeRef.getVersion()));
                result = false;
                continue;
            }

            String typeVersion = typeRef.getVersion();
            String defVersion = definition.getVersion();
            String resolved = "";
            if (!typeVersion.equals(defVersion)) {
                resolved = String.format(" (resolved to '%s')", defVersion);
            }

            logger.debug(String.format("Service '%s' references service type '%s' in registry '%s' @ version '%s'%s - OK",
                    service.getName(), typeRef.getTypeName(), typeRef.getHost(), typeVersion, resolved));
        }
        return result;
    }

    private boolean verifySocketConnectivity(String topicName, List<SocketReference> sockets) {
        boolean valid = true;
        boolean hasSource = false;
        boolean hasTarget = false;
        for (SocketReference socket : sockets) {
            String serviceName = socket.getServiceName();
            String socketName = socket.getSocketName();
            InOut io = socket.getDirection();

            hasSource |= io.isOut();
            hasTarget |= !io.isOut();

            String st = io.isOut() ? "Source" : "Target";
            String dir = io.isOut() ? "output" : "input";

            ServiceInstance service = ccfDataSource.findServiceByName(serviceName);

            if (service == null) {
                logger.error(String.format("%s of topic '%s' attempts to connect to %s socket '%s' on non-existing service '%s'",
                        st, topicName, dir, socketName, serviceName));
                valid = false;
                continue;
            }

            IServiceDefinition serviceDef = serviceResolver.findServiceByType(service);
            Set<String> serviceSockets = Stream.concat(
                    Util.stream(serviceDef.getSockets()), Util.stream(service.getAdditionalSockets())
            ).map(SocketDefinition::getName).collect(Collectors.toSet());

            if (serviceSockets.contains(socketName)) {
                logger.debug(String.format("%s of topic '%s' connects to %s socket '%s' on service '%s' - OK",
                        st, topicName, dir, socketName, serviceName));
            } else {
                logger.error(String.format("%s of topic '%s' attempts to connect to non-existing %s socket '%s' on service '%s'",
                        st, topicName, dir, socketName, serviceName));
                valid = false;
            }
        }

        if (!hasSource) {
            logger.warn(String.format("Topic '%s' has no sources", topicName));
        }

        if (!hasTarget) {
            logger.warn(String.format("Topic '%s' has no targets", topicName));
        }

        return valid;
    }

    private boolean verifySocketNameUniqueness(ServiceInstance service) {
        IServiceDefinition serviceDef = serviceResolver.findServiceByType(service);
        String serviceName = service.getName();
        boolean valid = true;
        for (InOut io : InOut.values()) {
            Set<String> socketNameDuplicates = new HashSet<>();
            Set<String> socketNames = new HashSet<>();
            List<SocketDefinition> allSockets = new ArrayList<>(serviceDef.getSockets(io));
            allSockets.addAll(service.getAdditionalSockets(io));
            for (SocketDefinition socket : allSockets) {
                String socketName = socket.getName();
                if (socketNames.contains(socketName)) {
                    socketNameDuplicates.add(socketName);
                }
                socketNames.add(socketName);
                checkSocketConnection(serviceName, socketName, io);
            }
            valid &= socketNameDuplicates.isEmpty();
            for (String duplicate : socketNameDuplicates) {
                logger.error(String.format("%s socket '%s' of service '%s' exists multiple times",
                        Util.capitalize(io.getType()), duplicate, serviceName));
            }
        }
        return valid;
    }

    private void checkSocketConnection(String serviceName, String socketName, InOut io) {
        if (ccfDataSource.findTopicsByServiceSocket(serviceName, socketName).isEmpty()) {
            // this is just an informational message (warning level), not an error
            logger.warn(String.format("%s socket '%s' of service '%s' is not connected to any topics",
                    Util.capitalize(io.getType()), socketName, serviceName));
        }
    }

    private SocketDefinition getSocketDefByRef(ServiceInstance service, IServiceDefinition serviceDef, SocketReference socketRef) {
        Optional<SocketDefinition> opt =
                Stream.concat(
                        Util.stream(serviceDef.getSockets(socketRef.getDirection())),
                        Util.stream(service.getAdditionalSockets(socketRef.getDirection()))
                ).filter(s -> s.getName().equals(socketRef.getSocketName())).findFirst();
        return opt.orElse(null);
    }

    private Pair<Boolean, List<Pair<SchemaSource, SchemaSource>>> getAllSchemas(List<SocketReference> sockets) {
        List<Pair<SchemaSource, SchemaSource>> schemas = new ArrayList<>();
        boolean valid = true;

        for (SocketReference socket : sockets) {
            ServiceInstance service = ccfDataSource.findServiceByName(socket.getServiceName());

            if (service == null) {
                // service does not exist (has already been logged by verifySocketConnectivity)
                continue;
            }

            IServiceDefinition serviceDef = serviceResolver.findServiceByType(service);
            SocketDefinition socketDef = getSocketDefByRef(service, serviceDef, socket);

            if (socketDef == null) {
                // Socket does not exist (has already been logged by verifySocketConnectivity)
                continue;
            }

            URI ccfUri = ccfFile.toUri();
            SchemaSource keySrc = socketDef.getSchemaReference(SchemaKind.KEY, ccfUri).resolve(socket, serviceDef);
            SchemaSource valueSrc = socketDef.getSchemaReference(SchemaKind.VALUE, ccfUri).resolve(socket, serviceDef);

            if (keySrc != null && valueSrc != null) {
                schemas.add(new Pair<>(keySrc, valueSrc));
            } else {
                // resolving one (or both) of the schemas failed (has already been logged by SchemaSource.resolve)
                valid = false;
            }
        }

        return new Pair<>(valid, schemas);
    }

    private Schema getAvroSchema(SchemaSource schemaRef) {
        // we may have null values in the map (when schema retrieval has failed), so a containsKey check is necessary
        if (avroSchemaCache.containsKey(schemaRef)) {
            return avroSchemaCache.get(schemaRef);
        }

        Schema schema = null;
        try (InputStream istream = schemaRef.openStream()){
            schema = new Schema.Parser().parse(istream);
        } catch (Exception ex) {
            logger.error(String.format("Failed to retrieve schema from %s", schemaRef.toString()), ex);
        }
        // if retrieval failed, we store the resulting null in the map so we don't try to
        // download repeatedly from the same invalid URL
        avroSchemaCache.put(schemaRef, schema);
        return schema;
    }

    private boolean verifySchemaCompatibility(SchemaSource source, SchemaSource target) {
        Schema sourceSchema = getAvroSchema(source);
        Schema targetSchema = getAvroSchema(target);

        if (sourceSchema == null || targetSchema == null) {
            return false;
        }

        SchemaCompatibility.SchemaPairCompatibility comp = SchemaCompatibility.checkReaderWriterCompatibility(targetSchema, sourceSchema);
        if (comp.getType() == SchemaCompatibility.SchemaCompatibilityType.COMPATIBLE) {
            logger.debug(String.format("Source %s is compatible to target %s - OK", source, target));
            return true;
        }

        logger.error(String.format("Source %s is NOT compatible to target %s", source, target));
        return false;
    }

    private static class SchemaInfo {
        SchemaSource source;
        JsonNode parsed;
        String name;
        String namespace;
        long version;

        public String getFQN() {
            return String.format("%s:%s", namespace, name);
        }
    }

    private List<SchemaInfo> parseAll(List<Pair<SchemaSource, SchemaSource>> pairs, Function<Pair<SchemaSource, SchemaSource>, SchemaSource> selector) {
        ObjectMapper mapper = new ObjectMapper();

        List<SchemaInfo> result = new ArrayList<>();
        for (Pair<SchemaSource, SchemaSource> pair : pairs) {
            SchemaSource source = selector.apply(pair);
            try {
                SchemaInfo info = new SchemaInfo();
                info.source = source;
                info.parsed = mapper.readTree(source.openStream());

                String namespace = Util.sn(info.parsed.get("namespace"), JsonNode::textValue);
                info.namespace = namespace != null ? namespace : "";

                String name = Util.sn(info.parsed.get("name"), JsonNode::textValue);
                info.name = name != null ? name : "";

                Long version = Util.sn(info.parsed.get("version"), JsonNode::longValue);
                info.version = version != null ? version : 0;

                result.add(info);
            } catch (IOException ex) {
                logger.error("Error parsing schema from source " + source, ex);
            }
        }
        return result;
    }

    private boolean checkOnlyOneName(List<SchemaInfo> schemas, Topic topic, String kv) {
        Set<String> names = schemas.stream().map(SchemaInfo::getFQN).collect(Collectors.toSet());
        if (names.size() == 1) {
            // OK
            return true;
        } else {
            logger.error(String.format("Mismatched %s schema names on topic '%s': [%s]",
                    kv, topic.getName(), String.join(", ", names)));
            return false;
        }
    }

    private Pair<SchemaInfo, Boolean> getLatestSchema(List<SchemaInfo> schemas) {
        if (schemas.isEmpty()) {
            throw new IllegalArgumentException("Schema list is empty");
        }

        Map<Long, List<SchemaInfo>> byVersion = schemas.stream().collect(Collectors.groupingBy(i -> i.version, Collectors.toList()));
        boolean valid = true;
        for (Map.Entry<Long, List<SchemaInfo>> item : byVersion.entrySet()) {
            List<SchemaInfo> infos = item.getValue();
            if (infos.size() > 1) {
                SchemaInfo first = infos.get(0);
                for (int i = 1; i < infos.size(); ++i) {
                    SchemaInfo other = infos.get(i);
                    if (!first.parsed.equals(other.parsed)) {
                        logger.error(String.format("Schema %s @ version %d: source '%s' differs from source '%s'",
                                first.getFQN(), first.version, first.source, other.source));
                        valid = false;
                    }
                }
            }
        }

        Optional<Map.Entry<Long, List<SchemaInfo>>> max = byVersion.entrySet().stream().max(Comparator.comparingLong(Map.Entry::getKey));
        if (!max.isPresent()) {
            logger.error("Maximum does not exist (this should not be possible)");
            throw new RuntimeException();
        }
        SchemaInfo latest = max.get().getValue().get(0);
        return new Pair<>(latest, valid);
    }

    private boolean verifySchemaCompatibility(Topic topic) {
        boolean valid;
        Pair<Boolean, List<Pair<SchemaSource, SchemaSource>>> pair;

        pair = getAllSchemas(topic.getSockets(InOut.OUT));
        List<Pair<SchemaSource, SchemaSource>> sourceSchemas = pair.right();
        valid = pair.left();

        pair = getAllSchemas(topic.getSockets(InOut.IN));
        List<Pair<SchemaSource, SchemaSource>> targetSchemas = pair.right();
        valid &= pair.left();

        List<Pair<SchemaSource, SchemaSource>> allSchemas = new ArrayList<>(sourceSchemas);
        allSchemas.addAll(targetSchemas);

        if (allSchemas.isEmpty()) {
            logger.error(String.format("Topic '%s' has no schemas", topic.getName()));
            return false;
        }

        List<SchemaInfo> allKeySchemasParsed = parseAll(allSchemas, Pair::left);
        List<SchemaInfo> allValueSchemasParsed = parseAll(allSchemas, Pair::right);

        // verify that all the schemas on this topic have the same type (i.e. fully-qualified name)
        valid &= checkOnlyOneName(allKeySchemasParsed, topic, "key");
        valid &= checkOnlyOneName(allValueSchemasParsed, topic, "value");

        // every source schema must be compatible to every target schema
        for(Pair<SchemaSource, SchemaSource> source : sourceSchemas) {
            for (Pair<SchemaSource, SchemaSource> target : targetSchemas) {
                valid &= verifySchemaCompatibility(source.left(), target.left());
                valid &= verifySchemaCompatibility(source.right(), target.right());
            }
        }

        Pair<SchemaInfo, Boolean> latestKey = getLatestSchema(allKeySchemasParsed);
        valid &= latestKey.right();
        Pair<SchemaInfo, Boolean> latestValue = getLatestSchema(allValueSchemasParsed);
        valid &= latestValue.right();
        if (valid) {
            topic.setCanonicalSchemas(new Pair<>(latestKey.left().source, latestValue.left().source));
        }

        return valid;
    }

    private boolean verifyServiceCredentials() {
        boolean valid = true;
        for (ServiceInstance service : ccfDataSource.getAllServices()) {
            IServiceDefinition definition = serviceResolver.findServiceByType(service);
            List<Credential> credentials = definition.getCredentials();
            for (Credential credential : credentials) {
                if (credential.isRequired() && !credential.isProvided()) {
                    logger.error(String.format("Credential '%s' is required by service '%s' but not provided by Composer",
                            credential.getName(), service.getName()));
                    valid = false;
                }
            }
        }
        return valid;
    }

    private boolean verifyServiceConfigs() {
        boolean valid = true;
        for (ServiceInstance service : ccfDataSource.getAllServices()) {
            IServiceDefinition definition = serviceResolver.findServiceByType(service);
            for (Parameter param : definition.getParameters()) {
                JsonSchema paramSchema;
                try {
                    paramSchema = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V7).getSchema(param.getSchemaJson());
                } catch (JsonSchemaException ex) {
                    logger.error(String.format("Invalid configuration schema for parameter '%s' of service type '%s'",
                            param.getName(), definition.getServiceTypeRef()), ex);
                    valid = false;
                    continue;
                }

                JsonNode config = service.getParameterJson().get(param.getName());
                if (config == null) {
                    if (param.isRequired() && !param.isProvided()) {
                        logger.error(String.format("Missing configuration for required parameter '%s' of service '%s'",
                                param.getName(), service.getName()));
                        valid = false;
                    }
                } else {
                    if (param.isProvided()) {
                        logger.error(String.format("Configuration contains value for parameter '%s' of service type '%s', but this parameter is provided by Composer",
                                param.getName(), definition.getServiceTypeRef()));
                        valid = false;
                        continue;
                    }
                    Set<ValidationMessage> result = paramSchema.validate(config);
                    if (!result.isEmpty()) {
                        logger.error(String.format("Configuration schema validation failed for parameter '%s' of service '%s'",
                                param.getName(), service.getName()));
                        for (ValidationMessage msg : result) {
                            logger.error(msg.toString());
                        }
                        valid = false;
                    }
                }
            }
        }
        return valid;
    }

    private boolean verifyConnectors() {
        boolean valid = true;

        for (ServiceInstance service : ccfDataSource.getAllServices()) {
            Set<String> names = new HashSet<>();
            for (Connector connector : service.getConnectors()) {
                String socketName = connector.getName();
                if (names.contains(socketName)) {
                    logger.error(String.format("Multiple connector definitions found with name '%s' for service '%s'", socketName, service.getName()));
                    valid = false;
                }
                names.add(socketName);
            }
        }

        return valid;
    }

    private boolean verifySockets() {
        boolean valid = true;

        for (ServiceInstance service : ccfDataSource.getAllServices()) {
            valid &= verifySocketNameUniqueness(service);
            valid &= verifyAdditionalSockets(service);
        }

        for (Topic topic : ccfDataSource.getAllTopics()) {
            String topicName = topic.getName();
            valid &= verifySocketConnectivity(topicName, topic.getSockets());
            valid &= verifySchemaCompatibility(topic);
        }
        return valid;
    }

    private boolean verifyAdditionalSockets(ServiceInstance service) {
        boolean valid = true;
        Set<String> unusedConnectors = service.getConnectors().stream().map(Connector::getName).collect(Collectors.toSet());
        for (AdditionalSocket additionalSocket : service.getAdditionalSockets()) {
            AdditionalSocketVerificationVisitor visitor = new AdditionalSocketVerificationVisitor(service);
            additionalSocket.visit(visitor);
            valid &= visitor.isValid();
            unusedConnectors.removeAll(visitor.getUsedConnectors());
        }
        if (!unusedConnectors.isEmpty()) {
            logger.warn(String.format("The following connectors on service '%s' are unused: %s", service.getName(), unusedConnectors));
        }
        return valid;
    }

    private static class AdditionalSocketVerificationVisitor implements IAdditionalSocketVisitor {
        private final ServiceInstance service;
        private Boolean valid = null;
        private final Set<String> usedConnectors = new HashSet<>();

        public AdditionalSocketVerificationVisitor(ServiceInstance service) {
            this.service = service;
        }

        @Override
        public void visitBuiltin(AdditionalSocketBuiltin socket) {
            guardReuse();
            // no specific validation necessary
            valid = true;
        }

        @Override
        public void visitConnect(AdditionalSocketConnect socket) {
            guardReuse();
            String connectorRef = socket.getConnectorRef();
            for (Connector connector : service.getConnectors()) {
                if (connector.getName().equals(connectorRef)) {
                    valid = true;
                    usedConnectors.add(connectorRef);
                    return;
                }
            }
            logger.error(String.format("No connector '%s' found for %s socket '%s' on service '%s'", connectorRef,
                    socket.getDirection().getType(), socket.getName(), service.getName()));
            valid = false;
        }

        public boolean isValid() {
            if (valid == null) {
                throw new IllegalStateException("The visitor has not been used yet");
            }
            return valid;
        }

        public Set<String> getUsedConnectors() {
            return usedConnectors;
        }

        private void guardReuse() {
            if (valid != null) {
                throw new IllegalStateException("This visitor has already been used");
            }
        }
    }

    public static void process(Path ccfFile, AuthData authData, boolean skipValidation, IGenerator generator, AuthMethod authMethod) {
        new Composer(ccfFile, authData, skipValidation, generator, authMethod).process();
    }

    private void process() {
        logger.info(String.format("Parsing CCF file '%s'", ccfFile));
        JsonNode ccf = Util.parseJson(ccfFile);
        logger.debug("Parsed successfully");
        URI schemaUri = getSchemaUri(ccf);
        if (!skipValidation) {
            logger.info(String.format("Validating CCF (schema: '%s')", schemaUri));
            JsonSchema schema = getJsonSchema(schemaUri);
            Set<ValidationMessage> result = schema.validate(ccf);
            if (!result.isEmpty()) {
                logger.error("CCF schema validation failed");
                for (ValidationMessage msg : result) {
                    logger.error(msg.toString());
                }
                System.exit(1);
            }
            logger.debug("CCF schema validation successful");
        }
        ccfDataSource = new CCFDataSource(ccf);

        logger.info("Checking service references");
        if (!verifyServicesExist()) {
            logger.error("Unresolved service references found");
            System.exit(1);
        }
        logger.debug("Service reference check successful");

        long needAuth = ccfDataSource.getAllServices().stream()
                .map(serviceResolver::findServiceByType)
                .filter(IServiceDefinition::needsAuth)
                .count();

        if (needAuth > 0 && authMethod == AuthMethod.NONE) {
            logger.error("Some of the services are located in repositories that require authentication to access.");
            logger.error("No method for handling this authentication has been specified.");
            logger.error("Please use either the --copy-auth or the --assume-auth option when running Composer.");
            System.exit(1);
        }

        logger.info("Checking service credentials");
        if (!verifyServiceCredentials()) {
            logger.error("Invalid service credentials found");
            System.exit(1);
        }
        logger.debug("Service credential check successful");

        logger.info("Checking service configurations");
        if (!verifyServiceConfigs()) {
            logger.error("Invalid service configuration found");
            System.exit(1);
        }
        logger.debug("Service configuration check successful");

        logger.info("Checking service connector definitions");
        if (!verifyConnectors()) {
            logger.error("Invalid service connector definitions found");
            System.exit(1);
        }
        logger.debug("Service connector definition check successful");

        logger.info("Checking socket configuration");
        if (!verifySockets()) {
            logger.error("Problems with socket configuration found");
            System.exit(1);
        }
        logger.debug("Socket configuration check successful");

        if (!generator.generate(ccfDataSource, serviceResolver, authMethod)) {
            logger.error("Output generation failed");
            System.exit(1);
        }
        logger.info(String.format("Service configuration file '%s' generated successfully", generator.getOutput()));
    }
}
