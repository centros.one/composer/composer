package one.centros;

import one.centros.generator.GeneratorConfig;
import one.centros.generator.GeneratorType;
import one.centros.generator.IGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Model.OptionSpec;
import picocli.CommandLine.ParseResult;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Main {
    private enum LogLevel {
        TRACE, DEBUG, INFO, WARN, ERROR
    }

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private static final String OPTION_CCF = "ccf-file";
    private static final String ENVVAR_CCF = "COMPOSER_CCF_FILE";

    private static final String OPTION_AUTH_FILE = "auth-file";
    private static final String ENVVAR_AUTH_FILE = "COMPOSER_AUTH_FILE";

    private static final String OPTION_GENERATOR = "generator-type";
    private static final String ENVVAR_GENERATOR = "COMPOSER_GENERATOR_TYPE";

    private static final String OPTION_OUTPUT = "output-file";
    private static final String ENVVAR_OUTPUT = "COMPOSER_OUTPUT_FILE";

    private static final String OPTION_GENERATOR_CONFIG = "generator-config";
    private static final String ENVVAR_GENERATOR_CONFIG = "COMPOSER_GENERATOR_CONFIG";

    private static final String OPTION_LOGLEVEL = "loglevel";
    private static final String ENVVAR_LOGLEVEL = "COMPOSER_LOGLEVEL";

    private static final String OPTION_SKIP_CCF_VALIDATION = "skip-ccf-validation";
    private static final String ENVVAR_SKIP_CCF_VALIDATION = "SKIP_CCF_VALIDATION";

    private static final String OPTION_ASSUME_AUTH = "assume-auth";
    private static final String ENVVAR_ASSUME_AUTH = "ASSUME_AUTH";

    private static final String OPTION_COPY_AUTH = "copy-auth";
    private static final String ENVVAR_COPY_AUTH = "COPY_AUTH";

    private static OptionSpec setup(String option, String label, Class<?> type, String envVar,
                                    String defVal, String... description) {
        return setup(option, label, type, new Class<?>[0], "", envVar, false, defVal, description);
    }

    private static OptionSpec setup(String option, String envVar, String defVal, String... description) {
        return setup(option, "", Boolean.class, new Class<?>[0], "", envVar, true, defVal, description);
    }

    private static OptionSpec setup(String option, String label, Class<?> type, Class<?>[] auxiliaryTypes, String splitRegex,
                                    String envVar, boolean optional, String defVal, String... description) {
        String envVal = System.getenv(envVar);
        List<String> desc = new ArrayList<>(Arrays.asList(description));
        desc.add("Environment variable: " + envVar);
        if (envVal != null) {
            desc.add("Environment value: " + envVal);
        } else {
            if (defVal != null) {
                desc.add("Default value: " + defVal);
            }
            envVal = defVal;
        }
        return OptionSpec.builder("--" + option)
                .paramLabel(label)
                .type(type)
                .auxiliaryTypes(auxiliaryTypes)
                .splitRegex(splitRegex)
                .required(!optional && envVal == null)
                .defaultValue(envVal)
                .description(desc.toArray(new String[0]))
                .build();
    }

    public static void main(String[] args) {
        CommandSpec spec = CommandSpec.create();
        spec.mixinStandardHelpOptions(true);
        spec.versionProvider(new VersionProvider());
        spec.usageMessage().header("All options can also be specified via the environment variables given below");
        spec.addOption(setup(OPTION_CCF, "INPUT", Path.class, ENVVAR_CCF, null,
                "The CCF input file to process"));
        spec.addOption(setup(OPTION_AUTH_FILE, "AUTHFILE", Path.class, ENVVAR_AUTH_FILE, "./authfile.json",
                "Name/path of the file holding authentication data for service repositories"));
        spec.addOption(setup(OPTION_GENERATOR, "GENERATOR", GeneratorType.class, ENVVAR_GENERATOR, GeneratorType.GENERIC.toString(),
                "Strategy for generating output", "Possible values: ${COMPLETION-CANDIDATES}"));
        spec.addOption(setup(OPTION_OUTPUT, "OUTFILE", Path.class, ENVVAR_OUTPUT, null,
                "Path and name for the output file to be generated"));
        spec.addOption(setup(OPTION_GENERATOR_CONFIG, "KEY=VALUE", Map.class, new Class<?>[] { String.class, String.class },
                ";", ENVVAR_GENERATOR_CONFIG, true, null, "Configuration options for the output generator",
                "Multiple KEY=VALUE pairs can alternatively be provided by repeated use of this option"));
        spec.addOption(setup(OPTION_LOGLEVEL, "LOGLEVEL", LogLevel.class, ENVVAR_LOGLEVEL, LogLevel.INFO.name(),
                "Log level", "Possible values: ${COMPLETION-CANDIDATES}"));
        spec.addOption(setup(OPTION_SKIP_CCF_VALIDATION, ENVVAR_SKIP_CCF_VALIDATION, Boolean.toString(false),
                "Skip the CCF validation step"));
        spec.addOption(setup(OPTION_ASSUME_AUTH, ENVVAR_ASSUME_AUTH, Boolean.toString(false),
                "Assume that correct authentication is provided on the target cluster (image pull secrets etc.)"));
        spec.addOption(setup(OPTION_COPY_AUTH, ENVVAR_COPY_AUTH, Boolean.toString(false),
                "Create image pull secrets for services that require authentication"));
        CommandLine cmdline = new CommandLine(spec);
        cmdline.setExecutionStrategy(Main::run);
        System.exit(cmdline.execute(args));
    }

    private static <T> T getValue(ParseResult parseResult, String option) {
        return parseResult.commandSpec().findOption(option).getValue();
    }

    private static int run(ParseResult parseResult) {
        LogLevel logLevel = getValue(parseResult, OPTION_LOGLEVEL);
        System.setProperty("org.slf4j.simpleLogger.log.one.centros", logLevel.name());

        // handle requests for help or version information
        Integer helpExitCode = CommandLine.executeHelpRequest(parseResult);
        if (helpExitCode != null) { return helpExitCode; }

        Path input = getValue(parseResult, OPTION_CCF);
        Path authfile = getValue(parseResult, OPTION_AUTH_FILE);

        // Note: matchedOption does not work for optional options
        GeneratorType genType = getValue(parseResult, OPTION_GENERATOR);
        IGenerator generator = null;
        try {
            generator = genType.getInstance();
        } catch (ReflectiveOperationException e) {
            // if we ever reach this branch, something strange has happened (picocli should only accept valid enum values)
            logger.error("Invalid generator type: " + genType, e);
            System.exit(1);
        }

        Path output = getValue(parseResult, OPTION_OUTPUT);
        // either the file must exist and be (over-)writeable, or the parent directory has to
        if (!(Files.isWritable(output) || Files.isWritable(output.toAbsolutePath().normalize().getParent()))) {
            logger.error(String.format("Output file '%s' is not writeable", output));
            System.exit(1);
        }
        generator.setOutput(output);

        Map<String, String> gc = getValue(parseResult, OPTION_GENERATOR_CONFIG);
        generator.configure(new GeneratorConfig(gc));

        AuthData authData;
        if (Files.exists(authfile)) {
            authData = AuthData.fromJson(Util.parseJson(authfile));
        } else {
            logger.warn(String.format("Auth file '%s' does not exist", authfile.toString()));
            authData = AuthData.EMPTY;
        }

        Composer.AuthMethod method;
        boolean copyAuth = getValue(parseResult, OPTION_COPY_AUTH);
        boolean assumeAuth = getValue(parseResult, OPTION_ASSUME_AUTH);
        if (copyAuth && assumeAuth) {
            logger.error(String.format("Both the options --%s and --%s have been given, but only one of them may be used.",
                    OPTION_COPY_AUTH, OPTION_ASSUME_AUTH));
            return 1;
        } else if (copyAuth) {
            logger.warn("The 'copy-auth' method for authentication handling has been selected. Be aware\n" +
                        "\tthat this means that authentication credentials from your authfile may be used\n" +
                        "\tfor creating the image-pull-secrets for all services whose images are not\n" +
                        "\tpublicly accessible. Make sure this is actually what you want.");
            method = Composer.AuthMethod.COPY;
        } else if (assumeAuth) {
            method = Composer.AuthMethod.ASSUME;
        } else {
            method = Composer.AuthMethod.NONE;
        }

        try {
            Composer.process(input, authData, getValue(parseResult, OPTION_SKIP_CCF_VALIDATION), generator, method);
        } catch (Throwable ex) {
            logger.error("Uncaught exception", ex);
            return 1;
        }
        return 0;
    }
}
