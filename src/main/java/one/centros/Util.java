package one.centros;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Spliterators;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Util {
    private static final Logger logger = LoggerFactory.getLogger(Util.class);

    private Util() { }

    public static JsonNode parseJson(Path input) {
        JsonNode root = null;
        try (InputStream instream = Files.newInputStream(input)) {
            ObjectMapper mapper = new ObjectMapper();
            root = mapper.readTree(instream);
        } catch (IOException ex) {
            logger.error(String.format("Failed to open json file '%s' for reading", input.toString()), ex);
            System.exit(1);
        }
        return root;
    }

    public static <T> Iterable<T> iterable(Iterator<T> iterator) {
        return () -> iterator;
    }

    public static <T> Stream<T> stream(Iterable<T> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false);
    }

    public static <T> Stream<T> stream(Iterator<T> iterator) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, 0), false);
    }

    /**
     * Because java lacks the "safe navigation" operator (?.) that other languages have, this is the replacement.
     * If {@code s} is {@code null}, the method returns {@code null}, otherwise it returns the value of {@code f}
     * applied to {@code s}.
     * Using lambdas this makes it possible for example to write
     * <pre>map?.get(key)</pre>
     * as the only moderately more verbose
     * <pre>sn(map, x -> x.get(key)</pre>
     * which is at least a little bit better than
     * <pre>map != null ? map.get(key) : null</pre>
     */
    public static <S, T> T sn(S s, Function<S, T> f) {
        return s != null ? f.apply(s) : null;
    }

    public static <S, T, U> U sn(S s, Function<S, T> f, Function<T, U> g) {
        T t = sn(s, f);
        return f != null ? g.apply(t) : null;
    }

    public static String capitalize(String str) {
        return str.substring(0,1).toUpperCase() + str.substring(1);
    }

    public static class First<T> implements Collector<T, First.Ref<T>, T> {
        private static class Ref<T> {
            T value;
        }

        @Override
        public Supplier<Ref<T>> supplier() {
            return Ref::new;
        }

        @Override
        public BiConsumer<Ref<T>, T> accumulator() {
            return (r, t) -> { if (r.value == null) r.value = t; };
        }

        @Override
        public BinaryOperator<Ref<T>> combiner() {
            return (r, s) -> r.value != null ? r : s;
        }

        @Override
        public Function<Ref<T>, T> finisher() {
            return r -> r.value;
        }

        @Override
        public Set<Characteristics> characteristics() {
            return EnumSet.of(Characteristics.UNORDERED);
        }
    }
}
