package one.centros.visitor;

import one.centros.model.AdditionalSocketBuiltin;
import one.centros.model.AdditionalSocketConnect;

public interface IAdditionalSocketVisitor {
    void visitBuiltin(AdditionalSocketBuiltin socket);
    void visitConnect(AdditionalSocketConnect socket);
}
