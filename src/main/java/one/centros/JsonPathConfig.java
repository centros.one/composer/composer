package one.centros;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;

import java.util.EnumSet;
import java.util.Set;

public class JsonPathConfig implements Configuration.Defaults {
    private final JsonProvider jsonProvider = new JacksonJsonNodeJsonProvider();
    private final MappingProvider mappingProvider = new JacksonMappingProvider();
    private final Set<Option> options = EnumSet.noneOf(Option.class);

    @Override
    public JsonProvider jsonProvider() {
        return jsonProvider;
    }

    @Override
    public MappingProvider mappingProvider() {
        return mappingProvider;
    }

    @Override
    public Set<Option> options() {
        return options;
    }
}
