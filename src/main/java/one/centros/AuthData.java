package one.centros;

import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

// example for auth file format:
// [
//   {
//     "host": "core.harbor.clouster.io",
//     "user": "admin",
//     "pass": "[SECRET]"
//   }
// ]

public class AuthData {
    public static final class Item {
        private final String host;
        private final String user;
        private final String pass;

        public Item(String host, String user, String pass) {
            this.host = host;
            this.user = user;
            this.pass = pass;
        }

        public String host() {
            return host;
        }

        public String user() {
            return user;
        }

        public String pass() {
            return pass;
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(AuthData.class);

    public static final AuthData EMPTY = new AuthData(Collections.emptyMap());

    private final Map<String, Item> data;

    private AuthData(Map<String, Item> data) {
        this.data = data;
    }

    public static AuthData fromJson(JsonNode root) {
        if (!root.isArray()) {
            logger.error("Malformed authdata file: not an array");
            System.exit(1);
        }

        Map<String, Item> map = new HashMap<>();
        for (JsonNode n : Util.iterable(root.elements())) {
            String host = n.get("host").textValue();
            if (host == null || host.isEmpty()) {
                logger.error("Malformed authdata file: 'host' missing from entry");
                System.exit(1);
            }
            String user = n.get("user").textValue();
            if (user == null || user.isEmpty()) {
                logger.error(String.format("Malformed authdata file: 'user' missing from entry for host '%s'", host));
                System.exit(1);
            }
            String pass = n.get("pass").textValue();
            if (pass == null || pass.isEmpty()) {
                logger.error(String.format("Malformed authdata file: 'pass' missing from entry for host '%s'", host));
                System.exit(1);
            }
            Item prev = map.put(host, new Item(host, user, pass));
            if (prev != null) {
                logger.warn(String.format("Malformed authdata file: duplicate entry for host '%s'", host));
            }
        }

        return new AuthData(map);
    }

    public Item getAuth(String host) {
        return data.get(host);
    }
}
