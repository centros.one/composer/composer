package one.centros.model;

public enum InOut {
    IN("input", "targets"),
    OUT("output", "sources");

    private final String type;
    private final String connectedBy;

    private InOut(String type, String connectedBy) {
        this.type = type;
        this.connectedBy = connectedBy;
    }

    public String getType() {
        return type;
    }

    public String getConnectedBy() {
        return connectedBy;
    }

    public boolean isOut() {
        return this == OUT;
    }
}
