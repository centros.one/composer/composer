package one.centros.model;

import com.fasterxml.jackson.databind.JsonNode;
import one.centros.visitor.IAdditionalSocketVisitor;

import java.util.HashMap;
import java.util.Map;

public abstract class AdditionalSocket extends SocketDefinition {

    public enum Type {
        BUILTIN("builtin"),
        CONNECT("connect");

        private String type;

        Type(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

        private static final Map<String, Type> lookup = new HashMap<>();

        static {
            for (Type type : Type.values()) {
                lookup.put(type.getType(), type);
            }
        }

        public static Type get(String type) {
            if (!lookup.containsKey(type)) {
                throw new IllegalArgumentException("Unknown type: " + type);
            }
            return lookup.get(type);
        }
    }

    public AdditionalSocket(InOut io, JsonNode socketNode) {
        super(io, socketNode);
    }

    public static AdditionalSocket fromJson(InOut io, JsonNode socketNode) {
        String value = socketNode.get("type").textValue();
        Type type = Type.get(value);
        switch (type) {
            case BUILTIN:
                return new AdditionalSocketBuiltin(io, socketNode);
            case CONNECT:
                return new AdditionalSocketConnect(io, socketNode);
        }
        throw new RuntimeException("This should never be reached, something weird happened");
    }

    public Type getType() {
        String value = socketNode.get("type").textValue();
        return Type.get(value);
    }

    public abstract void visit(IAdditionalSocketVisitor visitor);
}
