package one.centros.model;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Credential {
    private static final List<Pattern> PROVIDED_CREDENTIALS = new ArrayList<>();
    static {
        PROVIDED_CREDENTIALS.add(Pattern.compile("kubeconfig"));
    }

    private final String name;
    private final JsonNode definition;
    private final boolean required;

    public Credential(String name, JsonNode definition, boolean required) {
        this.name = name;
        this.definition = definition;
        this.required = required;
    }

    public String getName() {
        return name;
    }

    public JsonNode getDefinitionJson() {
        return definition;
    }

    public boolean isRequired() {
        return required;
    }

    public boolean isProvided() {
        return PROVIDED_CREDENTIALS.stream().anyMatch(p->p.matcher(name).matches());
    }
}
