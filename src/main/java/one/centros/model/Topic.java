package one.centros.model;

import com.fasterxml.jackson.databind.JsonNode;
import one.centros.Pair;
import one.centros.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/** Topic defined in CCF */
public class Topic {
    private final JsonNode topicNode;
    private Pair<SchemaSource, SchemaSource> canonicalSchemas;

    public Topic(JsonNode topicNode) {
        this.topicNode = topicNode;
    }

    public static Topic fromJson(JsonNode topicNode) {
        return new Topic(topicNode);
    }

    public String getName() {
        return topicNode.get("name").textValue();
    }

    public List<SocketReference> getSockets(InOut io) {
        return Util.stream(topicNode.get(io.getConnectedBy()))
                .map(n -> SocketReference.fromJson(io, n))
                .collect(Collectors.toList());
    }

    public List<SocketReference> getSockets() {
        List<SocketReference> socketNodes = new ArrayList<>();
        for (InOut io : InOut.values()) {
            socketNodes.addAll(getSockets(io));
        }
        return socketNodes;
    }

    public JsonNode getConfig() {
        return topicNode.get("config");
    }

    public void setCanonicalSchemas(Pair<SchemaSource, SchemaSource> canonicalSchemas) {
        this.canonicalSchemas = canonicalSchemas;
    }

    public Pair<SchemaSource, SchemaSource> getCanonicalSchemas() {
        return canonicalSchemas;
    }
}
