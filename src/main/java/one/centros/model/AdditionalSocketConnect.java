package one.centros.model;

import com.fasterxml.jackson.databind.JsonNode;
import one.centros.visitor.IAdditionalSocketVisitor;

public class AdditionalSocketConnect extends AdditionalSocket {
    public AdditionalSocketConnect(InOut io, JsonNode socketNode) {
        super(io, socketNode);
    }

    public String getConnectorRef() {
        return socketNode.get("connectorRef").textValue();
    }

    @Override
    public void visit(IAdditionalSocketVisitor visitor) {
        visitor.visitConnect(this);
    }
}
