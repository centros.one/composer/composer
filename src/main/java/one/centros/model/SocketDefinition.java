package one.centros.model;

import com.fasterxml.jackson.databind.JsonNode;

import java.net.URI;

/** Socket defined in CSCF */
public class SocketDefinition {
    private final InOut io;
    protected final JsonNode socketNode;

    public SocketDefinition(InOut io, JsonNode socketNode) {
        this.io = io;
        this.socketNode = socketNode;
    }

    public static SocketDefinition fromJson(InOut io, JsonNode socketNode) {
        return new SocketDefinition(io, socketNode);
    }

    public InOut getDirection() {
        return io;
    }

    public String getName() {
        return socketNode.get("name").textValue();
    }

    public SchemaSource getSchemaReference(SchemaKind schemaKind, URI baseUri) {
        // TODO: handle different schema types if we ever use anything other than avro
        JsonNode schemaNode = socketNode.get(schemaKind.getFieldname());
        String type = schemaNode.get("type").textValue();
        switch (type) {
            case "avro":
                return new SchemaUri(schemaKind, schemaNode.get("uri").textValue(), baseUri);
            case "avro-inline":
                return new SchemaInline(schemaKind, schemaNode.get("schema").toString(), getName());
            default:
                throw new IllegalArgumentException(String.format("Unknown schema type '%s'", type));
        }
    }
}
