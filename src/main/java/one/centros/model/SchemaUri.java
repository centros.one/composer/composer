package one.centros.model;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Objects;

public class SchemaUri implements SchemaSource {
    private static final Logger logger = LoggerFactory.getLogger(SchemaUri.class);

    private final SchemaKind kind;
    private final String schemaUri;
    private final URI baseUri;

    private URL resolvedUrl;
    private byte[] content;

    public SchemaUri(SchemaKind kind, String uri, URI baseUri) {
        this.kind = kind;
        this.schemaUri = uri;
        this.baseUri = baseUri;
    }

    @Override
    public boolean needsResolving() {
        return resolvedUrl == null;
    }

    @Override
    public SchemaSource resolve(SocketReference socketRef, IServiceDefinition serviceDef) {
        URI uri;
        try {
            uri = new URI(schemaUri);
        } catch (URISyntaxException ex) {
            logger.error(String.format("Invalid %s schema URI '%s' for %s socket '%s' of service '%s'",
                    kind,
                    schemaUri,
                    socketRef.getDirection().getType(),
                    socketRef.getSocketName(),
                    socketRef.getServiceName()), ex);
            return null;
        }

        try {
            resolvedUrl = uri.isAbsolute() ? uri.toURL() : baseUri.resolve(uri).toURL();
        } catch (MalformedURLException ex) {
            logger.error(String.format("Failed to resolve schema URI '%s'", uri), ex);
            return null;
        }

        return this;
    }

    @Override
    public String toString() {
        if (needsResolving()) {
            return String.format("%s schema @ URI '%s' (unresolved)", kind.toString(), schemaUri);
        }
        return String.format("%s schema @ URL '%s'", kind.toString(), resolvedUrl.toString());
    }

    public InputStream openStream() throws IOException {
        if (resolvedUrl != null) {
            if (content == null) {
                try (InputStream istream = resolvedUrl.openStream()) {
                    content = IOUtils.toByteArray(istream);
                }
            }
            return new ByteArrayInputStream(content);
        }
        throw new IOException("Unable to open unresolved Schema source");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SchemaUri schemaUri = (SchemaUri) o;
        return Objects.equals(resolvedUrl, schemaUri.resolvedUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(resolvedUrl);
    }
}
