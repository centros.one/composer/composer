package one.centros.model;

public enum SchemaKind {
    KEY ("key", "keySchema"), VALUE ("value", "valueSchema");

    private final String displayValue;
    private final String fieldname;

    private SchemaKind(String displayValue, String fieldname) {
        this.displayValue = displayValue;
        this.fieldname = fieldname;
    }

    @Override
    public String toString() {
        return displayValue;
    }

    public String getFieldname() {
        return fieldname;
    }
}
