package one.centros.model;

import com.fasterxml.jackson.databind.JsonNode;

public class Connector {
    private final JsonNode connectorNode;

    public Connector(JsonNode connectorNode) {
        this.connectorNode = connectorNode;
    }

    public static Connector fromJson(JsonNode connectorNode) {
        return new Connector(connectorNode);
    }

    public JsonNode getConfig() {
        return connectorNode.get("config");
    }

    public String getName() {
        return connectorNode.get("name").textValue();
    }
}
