package one.centros.model;

import java.io.IOException;
import java.io.InputStream;

public interface SchemaSource {
    boolean needsResolving();
    SchemaSource resolve(SocketReference socketRef, IServiceDefinition serviceDef);
    InputStream openStream() throws IOException;
}
