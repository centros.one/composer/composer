package one.centros.model;

import com.fasterxml.jackson.databind.JsonNode;

/** Socket referenced from a topic in CCF */
public class SocketReference {
    private final InOut io;
    private final JsonNode socketNode;

    public SocketReference(InOut io, JsonNode socketNode) {
        this.io = io;
        this.socketNode = socketNode;
    }

    public static SocketReference fromJson(InOut io, JsonNode socketNode) {
        return new SocketReference(io, socketNode);
    }

    public InOut getDirection() {
        return io;
    }

    public String getServiceName() {
        return socketNode.get("serviceRef").textValue();
    }

    public String getSocketName() {
        return socketNode.get("socket").textValue();
    }

    public String key() {
        return key(getServiceName(), getSocketName());
    }

    public static String key(String serviceName, String socketName) {
        return String.format("%s:%s", serviceName, socketName);
    }
}
