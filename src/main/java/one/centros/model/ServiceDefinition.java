package one.centros.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import one.centros.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ServiceDefinition implements IServiceDefinition {
    private final ServiceTypeRef typeRef;
    private final boolean needsAuth;
    private final DocumentContext context;

    public ServiceDefinition(ServiceTypeRef typeRef, JsonNode root, boolean needsAuth) {
        this.typeRef = typeRef;
        this.needsAuth = needsAuth;
        this.context = JsonPath.parse(root);
    }

    @Override
    public boolean needsAuth() {
        return needsAuth;
    }

    @Override
    public String getVersion() {
        return context.read("$.version", TextNode.class).textValue();
    }

    @Override
    public List<Parameter> getParameters() {
        List<Parameter> list = new ArrayList<>();
        JsonNode parametersNode = context.read("$.parameters");
        for (Map.Entry<String, JsonNode> item : Util.iterable(parametersNode.fields())) {
            String definitionName = item.getValue().get("definition").textValue();
            JsonNode definitionNode = context.read("$.definitions." + definitionName);
            JsonNode requiredNode = item.getValue().get("required");
            Parameter param = new Parameter(item.getKey(), definitionNode, requiredNode != null && requiredNode.booleanValue());
            list.add(param);
        }
        return list;
    }

    @Override
    public List<Credential> getCredentials() {
        List<Credential> list = new ArrayList<>();
        JsonNode credentialsNode = context.read("$.credentials");
        for (Map.Entry<String, JsonNode> item : Util.iterable(credentialsNode.fields())) {
            JsonNode definition = item.getValue();
            JsonNode required = definition.get("required");
            list.add(new Credential(item.getKey(), definition, required != null && required.booleanValue()));
        }
        return list;
    }

    @Override
    public List<SocketDefinition> getSockets(InOut io) {
        // Can't use dot (.) notation when one of the fields contains the dot as part of its name...
        String path = String.format("$['custom']['one.centros.csm']['sockets']['%s']", io.getType());
        List<SocketDefinition> sockets = new ArrayList<>();
        Object result = context.read(path);
        if (result instanceof ArrayNode) {
            for (JsonNode socketNode : (ArrayNode)result) {
                sockets.add(SocketDefinition.fromJson(io, socketNode));
            }
        }
        return sockets;
    }

    @Override
    public List<SocketDefinition> getSockets() {
        List<SocketDefinition> sockets = new ArrayList<>();
        for (InOut io : InOut.values()) {
            sockets.addAll(getSockets(io));
        }
        return sockets;
    }

    @Override
    public ServiceTypeRef getServiceTypeRef() {
        return typeRef;
    }
}
