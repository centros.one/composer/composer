package one.centros.model;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class SchemaInline implements SchemaSource {
    private final SchemaKind kind;
    private final String schema;
    private final String socketName;

    public SchemaInline(SchemaKind schemaKind, String schema, String socketName) {
        this.kind = schemaKind;
        this.schema = schema;
        this.socketName = socketName;
    }

    @Override
    public boolean needsResolving() {
        return false;
    }

    @Override
    public SchemaSource resolve(SocketReference socketRef, IServiceDefinition serviceDef) {
        return this;
    }

    @Override
    public InputStream openStream() {
        return new ByteArrayInputStream(schema.getBytes(StandardCharsets.UTF_8));
    }

    @Override
    public String toString() {
        return String.format("%s schema (inlined on socket '%s')", kind.toString(), socketName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SchemaInline that = (SchemaInline) o;
        return Objects.equals(schema, that.schema);
    }

    @Override
    public int hashCode() {
        return Objects.hash(schema);
    }
}
