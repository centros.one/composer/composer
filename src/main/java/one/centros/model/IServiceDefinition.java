package one.centros.model;

import java.util.Collections;
import java.util.List;

public interface IServiceDefinition {
    String getVersion();

    ServiceTypeRef getServiceTypeRef();

    default boolean needsAuth() {
        return false;
    }

    default List<Parameter> getParameters() {
        return Collections.emptyList();
    }

    default List<Credential> getCredentials() {
        return Collections.emptyList();
    }

    default List<SocketDefinition> getSockets(InOut io) {
        return Collections.emptyList();
    }

    default List<SocketDefinition> getSockets() {
        return Collections.emptyList();
    }
}
