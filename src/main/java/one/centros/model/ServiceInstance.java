package one.centros.model;

import com.fasterxml.jackson.databind.JsonNode;
import one.centros.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/** Service instance defined in CCF */
public class ServiceInstance {
    private final Pattern typeRegex = Pattern.compile("([^/]*)/(.*)");

    private final JsonNode serviceNode;

    public ServiceInstance(JsonNode serviceNode) {
        this.serviceNode = serviceNode;
    }

    public String getName() {
        return serviceNode.get("name").textValue();
    }

    public String getType() {
        return serviceNode.get("type").textValue();
    }

    public String getVersion() {
        return serviceNode.get("version").textValue();
    }

    public JsonNode getParameterJson() {
        return serviceNode.get("parameters");
    }

    public static ServiceInstance fromJson(JsonNode serviceNode) {
        return new ServiceInstance(serviceNode);
    }

    public ServiceTypeRef getTypeRef() {
        Matcher matcher = typeRegex.matcher(getType());
        if (matcher.matches()) {
            // type has an explicit repo id
            return new ServiceTypeRef(matcher.group(1), matcher.group(2), getVersion());
        } else {
            throw new IllegalArgumentException(String.format("Invalid service type '%s' for service '%s'", getType(), getName()));
        }
    }

    public List<Connector> getConnectors() {
        JsonNode node = serviceNode.get("connectors");
        if (node == null) { return Collections.emptyList(); }
        return Util.stream(node).map(Connector::fromJson).collect(Collectors.toList());
    }

    public List<AdditionalSocket> getAdditionalSockets(InOut io) {
        List<AdditionalSocket> sockets = new ArrayList<>();
        JsonNode nodes = Util.sn(serviceNode.get("additionalSockets"), n -> n.get(io.getType()));
        if (nodes != null) {
            for (JsonNode socketNode : nodes) {
                sockets.add(AdditionalSocket.fromJson(io, socketNode));
            }
        }
        return sockets;
    }

    public List<AdditionalSocket> getAdditionalSockets() {
        List<AdditionalSocket> sockets = new ArrayList<>();
        for (InOut io : InOut.values()) {
            sockets.addAll(getAdditionalSockets(io));
        }
        return sockets;
    }

    public boolean isProvided() {
        JsonNode node = serviceNode.get("provided");
        return node != null && node.booleanValue();
    }
}
