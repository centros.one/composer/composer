package one.centros.model;

import com.fasterxml.jackson.databind.JsonNode;
import one.centros.visitor.IAdditionalSocketVisitor;

public class AdditionalSocketBuiltin extends AdditionalSocket {
    public AdditionalSocketBuiltin(InOut io, JsonNode socketNode) {
        super(io, socketNode);
    }

    @Override
    public void visit(IAdditionalSocketVisitor visitor) {
        visitor.visitBuiltin(this);
    }
}
