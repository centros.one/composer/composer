package one.centros.model;

import java.util.Objects;

public class ServiceTypeRef {
    private final String host;
    private final String typeName;
    private final String version;

    public ServiceTypeRef(String host, String typeName, String version) {
        this.host = host;
        this.typeName = typeName;
        this.version = version;
    }

    public String getHost() {
        return host;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getVersion() {
        return version;
    }

    public String toString() {
        return String.format("%s:%s:%s", host, typeName, version);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServiceTypeRef that = (ServiceTypeRef) o;
        return host.equals(that.host) && typeName.equals(that.typeName) && version.equals(that.version);
    }

    @Override
    public int hashCode() {
        return Objects.hash(host, typeName, version);
    }
}
