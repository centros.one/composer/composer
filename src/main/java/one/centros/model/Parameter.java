package one.centros.model;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Parameter {
    private static final List<Pattern> PROVIDED_PARAMS = new ArrayList<>();
    static {
        PROVIDED_PARAMS.add(Pattern.compile("kafka-bootstrap"));
        PROVIDED_PARAMS.add(Pattern.compile("schema-registry-url"));
        PROVIDED_PARAMS.add(Pattern.compile("sockets-.*"));
    }

    private final String name;
    private final JsonNode schema;
    private final boolean required;

    public Parameter(String name, JsonNode schema, boolean required) {
        this.name = name;
        this.schema = schema;
        this.required = required;
    }

    public String getName() {
        return name;
    }

    public JsonNode getSchemaJson() {
        return schema;
    }

    public boolean isRequired() {
        return required;
    }

    public boolean isProvided() {
        return PROVIDED_PARAMS.stream().anyMatch(p->p.matcher(name).matches());
    }
}
