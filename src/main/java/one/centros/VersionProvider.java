package one.centros;

import picocli.CommandLine;

public class VersionProvider implements CommandLine.IVersionProvider {
    @Override
    public String[] getVersion() throws Exception {
        return new String[] { this.getClass().getPackage().getImplementationVersion() };
    }
}
