package one.centros.data;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import one.centros.Util;
import one.centros.model.ServiceInstance;
import one.centros.model.Topic;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CCFDataSource {
    private final DocumentContext context;
    private final List<ServiceInstance> allServices;
    private final List<Topic> allTopics;
    private final AncesTree ancestry;

    public CCFDataSource(JsonNode ccfRoot) {
        ancestry = new AncesTree(ccfRoot);
        context = JsonPath.parse(ccfRoot);
        allServices = Util.stream(context.read("$.services", ArrayNode.class))
                    .map(ServiceInstance::fromJson).collect(Collectors.toList());
        allTopics = Util.stream(context.read("$.topics", ArrayNode.class))
                .map(Topic::fromJson).collect(Collectors.toList());
    }

    public List<ServiceInstance> getAllServices() {
        return allServices;
    }

    public List<Topic> getAllTopics() {
        return allTopics;
    }

    public ServiceInstance findServiceByName(String name) {
        ArrayNode array = context.read("$.services[?(@.name=='" + name + "')]");
        JsonNode service = array.get(0);
        return service != null ? ServiceInstance.fromJson(service) : null;
    }

    public List<Topic> findTopicsByServiceSocket(String serviceName, String socketName) {
        List<Topic> topics = new ArrayList<>();
        ArrayNode sockets = context.read("$..[?(@.socket=='"+socketName+"'&&@.serviceRef=='"+serviceName+"')]");
        for (JsonNode socket : sockets) {
            JsonNode sourceOrTargetNode = ancestry.parentOf(socket);
            JsonNode topicNode = ancestry.parentOf(sourceOrTargetNode);
            topics.add(Topic.fromJson(topicNode));
        }
        return topics;
    }
}
