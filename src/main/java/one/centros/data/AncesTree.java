package one.centros.data;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.HashMap;
import java.util.Map;

// This class is necessary because JsonNode has no 'parent' property and therefore no way to navigate upwards from a
// node, but sometimes we still need to find an ancestor of a given node.
public class AncesTree {
    private final Map<JsonNode, JsonNode> ancestryMap;

    public AncesTree(JsonNode root) {
        ancestryMap = new HashMap<>();
        walk(root);
    }

    private void walk(JsonNode parent) {
        for (JsonNode child : parent) {
            walk(child);
            ancestryMap.put(child, parent);
        }
    }

    public JsonNode parentOf(JsonNode node) {
        return ancestryMap.get(node);
    }
}
